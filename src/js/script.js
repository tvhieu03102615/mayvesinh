"use strict";
jQuery(document).ready(function($){
  // Window on load
  $(window).on('load', function(){

    $('.slide-main .owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      items: 1
    });
    $(".owl-prev").html('<i class="fas fa-angle-left"></i>');
    $(".owl-next").html('<i class="fas fa-angle-right"></i>');

    $('.slide-tabs .owl-carousel').owlCarousel({
        items:1,
        loop:false,
        margin:10,
        URLhashListener:true,
        autoplay: false,
        startPosition: 'URLHash',
        dots: false
    });
    $('.image-to-show .owl-carousel').owlCarousel({
        items:1,
        loop:false,
        margin:5,
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash',
        dots: false
    });

    var listIcons = document.querySelectorAll(".list-icon");
    // console.log(listIcons);
    for(var i = 0; i < listIcons.length; i++) {
      var listIcon = listIcons[i];
      $(listIcon).click(function() {
        var menuRight = $(this).siblings('ul.list-relative-item').css('right');
        console.log(menuRight);
        if (menuRight == '2px') {
          $(this).siblings('ul.list-relative-item').css({
            right: '-100%'
          });
        }
        else {
          $(this).siblings('ul.list-relative-item').css({
            right: '2px'
          });
        }
        
      });
    }
    
    

  $(window).bind("scroll", function(e) {
    var top = $(window).scrollTop();
    if (top> 127) {
        $("nav#navigate-main").addClass("fixed");
      }
      else {
        $("nav#navigate-main").removeClass("fixed");
      } 
    });
  });
  if ($('.menu-page').length != 0) {
    var productViewstyles = $('.product_viewstyle li');
    $(productViewstyles).click(function(){
      $('.product_viewstyle li').removeClass('active');
      $(this).addClass('active');
    });
    $('#viewlist').click(function(){
      $('.menu-page .products-list').removeClass('active');
      $('.products-viewlist').addClass('active');
    });
    $('#viewgrid').click(function(){
      $('.menu-page .products-list').removeClass('active');
      $('.products-viewgrid').addClass('active');
    });
  }
  $("#send-address").validate({
    rules: {
      require_name:{
        required : true,
      },
      require_email: {
        required: true,
        email: true
      }
    },
    messages:{
      require_name:{
        required : "Vui lòng nhập tên",
      },
      require_email: {
        required: "Vui lòng nhập email",
        email: "Nhập đúng email của bạn"
      }
    },
  });
  if ($('#send-address').length != 0) {
    $('button#del').click(function(){
      $('#send-address input').val("");
      $('#send-address textarea').val("");
    })
  } 
  if ($('nav.menu-mobie').length != 0) {
    $('span.icon-menu').click(function(){
      $(this).closest('li').find('ul.menu-2').slideToggle();
      $(this).toggleClass('active');
      return false;
    });
  }
  if ($('nav.menu-mobie').length != 0) {
    $('.bar-mobie').click(function(){
      $('nav.menu-mobie').toggleClass('active');
      $('#wrapper').toggleClass('active');
    });
  }
  if ($('.part-title-menu').length != 0) {
    $('.part-title-menu .icon-show').click(function(){
      $(this).parent().find('ul').slideToggle();
    })
  }
});


